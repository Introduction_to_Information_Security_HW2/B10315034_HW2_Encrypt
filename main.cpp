//
//  main.cpp
//  B10315034_HW2
//
//  Created by Lung on 2015/11/5.
//  Copyright © 2015年 Allen LEE. All rights reserved.
//
#include <iostream>
#include <fstream>
#include <bitset>
#include <string>
#include <vector>
#include <time.h>
using namespace std;

bitset<48> tempkey[16];
string k = "1010111110101111101011111010111110101111101011111010111110101111";
string I = "1111101011111010111110101111101011111010111110101111101011111010";
bitset<64> key(k);
bitset<64> IV(I);
clock_t start , finish;
fstream file;
char fileName[32], RGBQUAD[640*480*3];
char text[8];
using std::fstream;
using std::ios;

typedef unsigned char   BYTE;
typedef unsigned short  WORD;
typedef unsigned long   DWORD;
typedef long            LONG;
WORD bfType;
DWORD bfSize;
WORD bfReserved1;
WORD bfReserved2;
DWORD bfOffBits;
DWORD biSize;
LONG biWidth;
LONG biHeight;
WORD biPlanes;
WORD biBitCount;
DWORD biCompression;
DWORD biSizeImage;
LONG biXPelsPerMeter;
LONG biYPelsPerMeter;
DWORD biClrUsed;
DWORD biClrImportant;


int key_table_1[] = {
    57, 49, 41, 33, 25, 17, 9,
    1, 58, 50, 42, 34, 26, 18,
    10, 2, 59, 51, 43, 35, 27,
    19, 11, 3, 60, 52, 44, 36,
    63, 55, 47, 39, 31, 23, 15,
    7, 62, 54, 46, 38, 30, 22,
    14, 6, 61, 53, 45, 37, 29,
    21, 13, 5, 28, 20, 12, 4};

int key_table_2[] = {
    14, 17, 11, 24, 1, 5,
    3, 28, 15, 6, 21, 10,
    23, 19, 12, 4, 26, 8,
    16, 7, 27, 20, 13, 2,
    41, 52, 31, 37, 47, 55,
    30, 40, 51, 45, 33, 48,
    44, 49, 39, 56, 34, 53,
    46, 42, 50, 36, 29, 32};

int shiftBits[] = {1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1};

bitset<28> leftshift(bitset<28> a , int shift) // 向左位移
{
    bitset<28> temp = a;
    for (int i=27 ;i>=0; i--)
    {
        if ((i-shift) < 0)
        {
            a[i] = temp[i-shift+28];
        }
        else
        {
            a[i] = temp[i-shift];
        }
    }
    return a;
}

void ProduceKey() // 產生金鑰
{
    bitset<56> key56;
    bitset<28> right;
    bitset<28> left;
    bitset<48> key48;
    
    for (int i=0; i<56; i++) // 使用 table1
    {
        key56[55-i] = key[64-key_table_1[i]];
    }
    
    for (int z = 0; z<16; z++)
    {
        for(int i=0; i<28; i++)
        {
            right[i] = key56[i];
        }
        for(int i=28; i<56; i++)
        {
            left[i-28] = key56[i];
        }
        
        left = leftshift(left , shiftBits[z]);
        right = leftshift(right, shiftBits[z]);
        
        for (int i=0; i<28; i++)
        {
            key56[i] = right[i];
        }
        for (int i=28; i<56; i++)
        {
            key56[i] = left[i-28];
        }
        for (int i=0; i<48; i++) // 56bits -> 48bits (table2)
        {
            key48[47-i] = key56[56-key_table_2[i]];
        }
        tempkey[z] = key48;
    }
}

bitset<64> char_to_bits(const char s[8]) // 字元轉位元
{
    bitset<64> bits;
    for (int i = 0; i<8; i++)
    {
        for (int j = 0; j<8; j++)
        {
            bits[i*8+j] = ((s[i]>>j) & 1);
        }
    }
    return bits;
}
char bitsettochar(bitset<64> text,int s) //位元轉字元
{
    int i = text[s]+text[s+1]*2+text[s+2]*4+text[s+3]*8+text[s+4]*16+text[s+5]*32+text[s+6]*64+text[s+7]*128;
    return i;
}
int IP[] = {
    58, 50, 42, 34, 26, 18, 10, 2,
    60, 52, 44, 36, 28, 20, 12, 4,
    62, 54, 46, 38, 30, 22, 14, 6,
    64, 56, 48, 40, 32, 24, 16, 8,
    57, 49, 41, 33, 25, 17, 9, 1,
    59, 51, 43, 35, 27, 19, 11, 3,
    61, 53, 45, 37, 29, 21, 13, 5,
    63, 55, 47, 39, 31, 23, 15, 7};


int IP_inverse[] = {
    40, 8, 48, 16, 56, 24, 64, 32,
    39, 7, 47, 15, 55, 23, 63, 31,
    38, 6, 46, 14, 54, 22, 62, 30,
    37, 5, 45, 13, 53, 21, 61, 29,
    36, 4, 44, 12, 52, 20, 60, 28,
    35, 3, 43, 11, 51, 19, 59, 27,
    34, 2, 42, 10, 50, 18, 58, 26,
    33, 1, 41, 9, 49, 17, 57, 25};
int Extend[] = {
    32, 1, 2, 3, 4, 5,
    4, 5, 6, 7, 8, 9,
    8, 9, 10, 11, 12, 13,
    12, 13, 14, 15, 16, 17,
    16, 17, 18, 19, 20, 21,
    20, 21, 22, 23, 24, 25,
    24, 25, 26, 27, 28, 29,
    28, 29, 30, 31, 32, 1};

int P[] = {
    16, 7, 20, 21,
    29, 12, 28, 17,
    1, 15, 23, 26,
    5, 18, 31, 10,
    2, 8, 24, 14,
    32, 27, 3, 9,
    19, 13, 30, 6,
    22, 11, 4, 25 };

int S_BOX[8][4][16] = {
    {
        {14,4,13,1,2,15,11,8,3,10,6,12,5,9,0,7},
        {0,15,7,4,14,2,13,1,10,6,12,11,9,5,3,8},
        {4,1,14,8,13,6,2,11,15,12,9,7,3,10,5,0},
        {15,12,8,2,4,9,1,7,5,11,3,14,10,0,6,13}
    },
    {
        {15,1,8,14,6,11,3,4,9,7,2,13,12,0,5,10},
        {3,13,4,7,15,2,8,14,12,0,1,10,6,9,11,5},
        {0,14,7,11,10,4,13,1,5,8,12,6,9,3,2,15},
        {13,8,10,1,3,15,4,2,11,6,7,12,0,5,14,9}
    },
    {
        {10,0,9,14,6,3,15,5,1,13,12,7,11,4,2,8},
        {13,7,0,9,3,4,6,10,2,8,5,14,12,11,15,1},
        {13,6,4,9,8,15,3,0,11,1,2,12,5,10,14,7},
        {1,10,13,0,6,9,8,7,4,15,14,3,11,5,2,12}
    },
    {
        {7,13,14,3,0,6,9,10,1,2,8,5,11,12,4,15},
        {13,8,11,5,6,15,0,3,4,7,2,12,1,10,14,9},
        {10,6,9,0,12,11,7,13,15,1,3,14,5,2,8,4},
        {3,15,0,6,10,1,13,8,9,4,5,11,12,7,2,14}
    },
    {
        {2,12,4,1,7,10,11,6,8,5,3,15,13,  0,14,9},
        {14,11,2,12,4,7,13,1,5,0,15,10,3,9,8,6},
        {4,2,1,11,10,13,7,8,15,9,12,5,6,3,0,14},
        {11,8,12,7,1,14,2,13,6,15,0,9,10,4,5,3}
    },
    {
        {12,1,10,15,9,2,6,8,0,13,3,4,14,7,5,11},
        {10,15,4,2,7,12,9,5,6,1,13,14,0,11,3,8},
        {9,14,15,5,2,8,12,3,7,0,4,10,1,13,11,6},
        {4,3,2,12,9,5,15,10,11,14,1,7,6,0,8,13}
    },
    {
        {4,11,2,14,15,0,8,13,3,12,9,7,5,10,6,1},
        {13,0,11,7,4,9,1,10,14,3,5,12,2,15,8,6},
        {1,4,11,13,12,3,7,14,10,15,6,8,0,5,9,2},
        {6,11,13,8,1,4,10,7,9,5,0,15,14,2,3,12}
    },
    {
        {13,2,8,4,6,15,11,1,10,9,3,14,5,0,12,7},
        {1,15,13,  8,10,3,7,4,12,5,6,11,0,14,9,2},
        {7,11,4,1,9,12,14,2,0,6,10,13,15,3,5,8},
        {2,1,14,7,4,10,8,13,15,12,9,0,3,5,6,11}
    }
};

bitset<32> F(bitset<32> r, bitset<48> k) // right 擴展
{
    bitset<48> expR;
    
    for(int i=0; i<48; ++i)
    {
        expR[47-i] = r[32-Extend[i]];
    }
    expR = expR ^ k;
    
    bitset<32> output;
    int x = 0;
    for(int i=0; i<48; i=i+6)
    {
        int row = expR[47-i]*2 + expR[47-i-5];
        int col = expR[47-i-1]*8 + expR[47-i-2]*4 + expR[47-i-3]*2 + expR[47-i-4];
        int num = S_BOX[i/6][row][col];
        
        bitset<4> binary(num);
        
        output[31-x] = binary[3];
        output[31-x-1] = binary[2];
        output[31-x-2] = binary[1];
        output[31-x-3] = binary[0];
        x += 4;
    }
    
    bitset<32> temp = output;
    for(int i=0; i<32; i++)
    {
        output[31-i] = temp[32-P[i]];
    }
    return output;
}

bitset<64>encrypt(bitset<64> & plain) // 加密
{
    bitset<64> cipher;
    bitset<64> tempbits;
    bitset<32> left;
    bitset<32> right;
    bitset<32> nleft;
    
    for (int i = 0; i<64; i++)
    {
        tempbits[63-i] = plain[64-IP[i]];
    }
    for(int i=0; i<32; i++)
    {
        right[i] = tempbits[i];
    }
    for(int i=32; i<64; i++)
    {
        left[i-32] = tempbits[i];
    }
    
    for(int z=0; z<16; z++)
    {
        nleft = right;
        right = left ^ F(right,tempkey[z]);
        left = nleft;
    }
    
    for(int i=0; i<32; i++)
    {
        cipher[i] = left[i];
    }
    for(int i=32; i<64; i++)
    {
        cipher[i] = right[i-32];
    }
    tempbits = cipher;
    
    for(int i=0; i<64; i++)
    {
        cipher[63-i] = tempbits[64-IP_inverse[i]];
    }
    return cipher;
}
bitset<64> check(bitset<64> iv , int z )    //CTR IV+1...IV+n
{
    bitset<64> a = z;
    bitset<1> c = 0;
    for(int i = 63; i>=0; i--)
    {
        if (iv.test(i) == true && a.test(i) == true )
        {
            if (c.test(0)== true)
            {
                iv.set(i);
                c.set(0);
            }
            else
            {
                iv.reset(i);
                c.set(0);
            }
            
        }
        else if(iv.test(i) == false && a.test(i) == false )
        {
            if (c.test(0) == true)
            {
                iv.set(i);
                c.reset(0);
            }
            else
            {
                iv.reset(i);
                c.reset(0);
            }
        }
        else if ((iv.test(i) == true && a.test(i)==false) || (iv.test(i) == false && a.test(i)==true) )
        {
            if (c.test(0)==true)
            {
                iv.reset(i);
                c.set(0);
            }
            else
            {
                iv.set(i);
                c.reset(0);
            }
        }
        
    }
    
    return iv;
}
void DES_encrypt() // DES 加密字串
{
    string s ;
    string k ;
    cout << "Enter your plaintext!! \n";
    cin >> s;
    cout << "Enter your key!! \n";
    cin >> k;
    
    bitset<64> plain =  char_to_bits(s.c_str());
    
    key = char_to_bits(k.c_str());
    
    ProduceKey();
    
    bitset<64> cipher = encrypt(plain);
    cout << cipher << endl;
}
void readbmp()                                  //讀圖檔
{
    int choose;
                                                //將圖檔從檔案輸入到記憶體
    cout << "Open FileName: ";
    cin >> fileName;
    
    file.open(fileName, ios::in|ios::binary);
    file.read((char*)&bfType, sizeof(WORD));
    file.read((char*)&bfSize, sizeof(DWORD));
    file.read((char*)&bfReserved1, sizeof(WORD));
    file.read((char*)&bfReserved2, sizeof(WORD));
    file.read((char*)&bfOffBits, sizeof(DWORD));
    file.read((char*)&biSize, sizeof(DWORD));
    file.read((char*)&biWidth, sizeof(LONG));
    file.read((char*)&biHeight, sizeof(LONG));
    file.read((char*)&biPlanes, sizeof(WORD));
    file.read((char*)&biBitCount, sizeof(WORD));
    file.read((char*)&biCompression, sizeof(DWORD));
    file.read((char*)&biSizeImage, sizeof(DWORD));
    file.read((char*)&biXPelsPerMeter, sizeof(LONG));
    file.read((char*)&biYPelsPerMeter, sizeof(LONG));
    file.read((char*)&biClrUsed, sizeof(DWORD));
    file.read((char*)&biClrImportant, sizeof(DWORD));
    file.read(RGBQUAD, sizeof(RGBQUAD));
    file.close();
    
                                                //將圖檔從記憶體輸出到檔案
    cout << "Enter you want to save name of file： ";
    cin >> fileName;
    
    file.open(fileName, ios::out|ios::binary);
    file.write((char*)&bfType, sizeof(WORD));
    file.write((char*)&bfSize, sizeof(DWORD));
    file.write((char*)&bfReserved1, sizeof(WORD));
    file.write((char*)&bfReserved2, sizeof(WORD));
    file.write((char*)&bfOffBits, sizeof(DWORD));
    file.write((char*)&biSize, sizeof(DWORD));
    file.write((char*)&biWidth, sizeof(LONG));
    file.write((char*)&biHeight, sizeof(LONG));
    file.write((char*)&biPlanes, sizeof(WORD));
    file.write((char*)&biBitCount, sizeof(WORD));
    file.write((char*)&biCompression, sizeof(DWORD));
    file.write((char*)&biSizeImage, sizeof(DWORD));
    file.write((char*)&biXPelsPerMeter, sizeof(LONG));
    file.write((char*)&biYPelsPerMeter, sizeof(LONG));
    file.write((char*)&biClrUsed, sizeof(DWORD));
    file.write((char*)&biClrImportant, sizeof(DWORD));
    
    start = clock();
    cout <<"Choose mod \n1.ECB\n2.CBC\n3.OFB\n4.CTR\n";
    cin >> choose;
    start= clock();
    if ( choose == 1 )                  //ECB
    {
        bitset<64> plain,cipher;
        
        for(int i=0;i<sizeof(RGBQUAD);i+=8)
        {
            plain = char_to_bits(&RGBQUAD[i]);
            cipher = encrypt(plain);
            
            for(int j=0;j<8;j++)
            {
                text[j]=bitsettochar(cipher,j*8);
            }
            file.write(text,sizeof(text));
        }
        
    }
    else if( choose == 2)                  //CBC
    {
        bitset<64> plain,cipher,temp;
        for (int i=0; i<8; i+=8)
        {
            plain = char_to_bits(&RGBQUAD[i]);
            plain ^= IV;
            cipher = encrypt(plain);
            temp = cipher;
            for (int j=0; j<8; j++)
            {
                text[j]=bitsettochar(cipher, j*8);
            }
            file.write(text, sizeof(text));
        }
        for(int i=8;i<sizeof(RGBQUAD);i+=8)
        {
            plain = char_to_bits(&RGBQUAD[i]);
            plain ^= temp;
            cipher = encrypt(plain);
            temp = cipher;
            
            for(int j=0;j<8;j++)
            {
                text[j]=bitsettochar(cipher,j*8);
            }
            file.write(text,sizeof(text));
        }
        
    }
    else if ( choose == 3 )                 //OFB2
    {
        bitset<64> plain,cipher,temp,ntemp;
        
            temp = encrypt(IV);
        for(int i=0;i<sizeof(RGBQUAD);i+=8)
        {
            plain = char_to_bits(&RGBQUAD[i]);
            cipher = temp ^ plain;
            for(int j=0;j<8;j++)
            {
                text[j]=bitsettochar(cipher,j*8);
            }
            file.write(text,sizeof(text));
            temp = encrypt(temp);
            
        }
        
    }
    else if ( choose == 4 )                 //CTR
    {
        bitset<64> plain,cipher,temp,iv1;
        
        
        for(int i=0 , z=1 ; i<sizeof(RGBQUAD) ; i+=8 )
        {
            temp = check(IV,z);
            iv1 = encrypt(temp);
            plain = char_to_bits(&RGBQUAD[i]);
            cipher = plain ^ iv1;
            
            for(int j=0;j<8;j++)
            {
                text[j]=bitsettochar(cipher,j*8);
            }
            file.write(text,sizeof(text));
            z++;
        }
        
    }
    finish = clock();
    cout<<"it's done!!\ntime:"<<(float)(finish - start)/CLOCKS_PER_SEC<<" s"<<endl;
    file.close();
}


int main() {
    // insert code here...
    int choose;
    cout << "Choose you want \n1.DES \n2.Des_Mod\n3.exit \n";
    cin >> choose;
    switch (choose)
    {
        case 1:
            DES_encrypt();
            break;
        case 2:
            readbmp();
            break;
        case 3:
            return 0;
            break;
    }
    
    return 0;
}
